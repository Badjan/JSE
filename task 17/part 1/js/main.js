"use strict";

var btn = document.getElementById("play");

function sliceArray (array, from, to) {
    array.splice(from, to);
}

function setDate(date) {
    return moment(date).format('YYYY/MM/DD HH:mm');
}

function setUrl(url) {
    return "http://" + url;
}

function print(data) {
    console.log(data);
}

function setNameCase(item) {
    return `${item.name.charAt(0).toUpperCase()}${item.name.slice(1).toLowerCase()}`;
}

function setParams(item) {
    return `${item.params.status} => ${item.params.progress}`;
}

function setDescription(item) {
    if (item.description.length > 15) return `${item.description.substr(0, 15)} ...`;
    return item.description;
}

function getRestructurisedArray(array, newArray) {
    return array.forEach( item => {
        newArray.push({
            url: item.url,
            name: item.name,
            params: item.params,
            description: item.description,
            date: item.date,
        })
    });
}

function getModifiedArray(array) {
    return array.map( item => {
        return {
            url: setUrl(item.url),
            name: setNameCase(item),
            params: setParams(item),
            description: setDescription(item),
            date: setDate(item.date),
            isVisible: item.params.status
        };
    });
}

function getFilteredArray(array) {
    return array.filter(item => item.isVisible);
}

function transform() {
    let newData = [],
        mapedData = [],
        filteredData = [];

    sliceArray(data, 5, 1);
    getRestructurisedArray(data, newData);

    mapedData = getModifiedArray(newData);
    filteredData = getFilteredArray(mapedData);

    print(filteredData);   
}

btn.addEventListener("click", transform);