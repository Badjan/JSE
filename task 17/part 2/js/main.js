"use strict";
function print(item) {
    console.log(item);
}

// 1 task

{
    let goods,
        stringFromArray;
    goods = ['foods', 'fruits', 'technics', 'phones', 'computers'];
    goods.splice(goods.indexOf('technics'), 1);
    stringFromArray = goods.join(", ");
    print(stringFromArray);
}

// 2 task

{
    let date = new Date(),
    dateString = `${date.getHours()}:${date.getMinutes()} ` + 
                 `${date.getDate() > 10 ? date.getDate() : '0' + date.getDate()}` + 
                 `/${date.getMonth() > 9 ? date.getMonth() + 1 : "0" + (date.getMonth() + 1)}` + 
                 `/${date.getFullYear()}`;
    print(dateString);
}

// 3 task

{
    let uri = "/home/user/project/script.js";

    function setResolution(string) {
        let regExp = /\./img,
            position = string.search(regExp);
        return string.slice(position);
    }

    print(setResolution(uri));
}

// 4 task

{
    let array = [1, 2, 2, 4, 5, 4, 7, 8, 7, 3, 6];
    
    function deleteDuplicate(arr) {
        for(let i = 0; i < arr.length; i++) {
            for(let j = i+1; j < arr.length; j++) {
                if(arr[i] === arr[j]) arr.splice(j, 1);
            }
        }
    }

    deleteDuplicate(array);

    print(array);
}