'use strict'

class BaseGallery {
	constructor () {
		this.displayedData = [];
		this.itemsOfGalleryContainer = document.querySelector('#itemsOfGalleryContainer');
		this.sortingOption = document.querySelector('#sel1');
		this.gallery = document.querySelector("#gallery");
		this.quantity = document.querySelector('#quantity');
		this.itemForm = document.querySelector('#itemForm');
		this.showItemFormBtn = document.querySelector('#showItemFormBtn');
		this.hideItemFormBtn = document.querySelector('#hideItemFormBtn');
		this.addItemBtn = document.querySelector('#addItem');
		this.updateItemBtn = document.querySelector('#updateItem');
		this.newItemId = document.querySelector('#newItemId');
	}


	/*Model */
	compareAlphabetical (a, b) {
		if (a.name.toLowerCase() > b.name.toLowerCase()) {
			return 1;
		} else {
			return -1;
		}
	}

	compareAlphabeticalReverse (a, b) {
		if (a.name.toLowerCase() < b.name.toLowerCase()) {
			return 1;
		} else {
			return -1;
		}
	}

	compareDate (a, b) {
		if (a.date > b.date) {
			return 1;
		} else {
			return -1
		}
	}

	compareDateReverse (a, b) {
		if (a.date < b.date) {
			return 1;
		} else {
			return -1
		}
	}
				
	setDate (date) {
		return moment(date).format('YYYY/MM/DD HH:mm');
	}
				
	setURL (url) {
		if(url.indexOf('http://') === 0 || url.indexOf('https://') === 0) {
			return url;
		} else {
			return `http://${url}`;
		}
	}
				
	setNameCase (item) {
		if (!item) return;
		return `${item.charAt(0).toUpperCase()}${item.slice(1).toLowerCase()}`;
	}
				
	setDescription (item) {
		if (item.length > 15) {
			return `${item.substr(0, 15)} ...`;
		} else {
			return item;
		}
	}
	
	renderResult (element, result) {
		element.innerHTML = result;
	}
				
	sortingArray () {
		let option = this.sortingOption.value;
		if (option === "0") {
			this.displayedData.sort(this.compareAlphabetical);
		} else if (option == 1) {
			this.displayedData.sort(this.compareAlphabeticalReverse);
		} else if (option == 2) {
			this.displayedData.sort(this.compareDateReverse);
		} else {
			this.displayedData.sort(this.compareDate);
		}
	}
				
	saveSortingOptionToLS () {
		localStorage.setItem("sortingOption", this.sortingOption.value);
	}
								
	fillingDataWithTemplateStrings () {
		let result = "";
		for (let i = 0; i < this.displayedData.length; i++) {
			let template = `<div class="col-md-4" id="${i}">\
								<div class="card mb-4 box-shadow">\
									<img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="${this.setNameCase(this.displayedData[i].name)}" src="${this.setURL(this.displayedData[i].url)}" data-holder-rendered="true" style="height: 225px; width: 100%; display: block;">\
									<div class="card-body">\
										<p class="card-text">${this.setDescription(this.displayedData[i].description)}</p>\
										<div class="d-flex justify-content-between align-items-center">\
											<a href="#itemForm">
												<div class="btn-group">\
													<button type="button" class="btn btn-outline-secondary" data-toggle-viewItem="${this.displayedData[i].id}">View</button>\
													<button type="button" class="btn btn-outline-secondary" data-toggle-editItem="${this.displayedData[i].id}">Edit</button>\
												</div>\
											</a>\
											<a class="btn btn-danger" data-toggle-id="${this.displayedData[i].id}">Удалить</a>\
											<small class="text-muted">${this.setDate(this.displayedData[i].date)}</small>\
										</div>\
									</div>\
								</div>\
							</div>`;
				
			result += template;
		}
		return result;
	}

	returnEmptyId () {
		let data = this.displayedData,
			arrWithIds = [];

		for (let value of data) {
			arrWithIds.push(value.id)
		} 

		for (let i = 1;;i++) {
			if (arrWithIds.indexOf(i) == -1) return i;
		}
	}

	setNewItemId () {
		this.newItemId.value = this.returnEmptyId();
	}

	createItem () {
		let	newItem = {
			url : document.querySelector('#newItemUrl').value,
			name : document.querySelector('#newItemName').value,
			id : this.returnEmptyId(),
			description : document.querySelector('#newItemDescription').value,
			date : moment(document.querySelector('#newItemDate').value).format('x')
		};
		
		let options = {
			method: 'post',
			headers: {"Content-type":"application/json;charset=UTF-8"}, 
			body: JSON.stringify(newItem)
		};

		fetch(`http://localhost:3000/cars`, options).then(responce => responce.json())
            .then(data => {
				this.initComponent();
            })
	}

	returnUpdatingItem (event, toggle) {
		let target = event.target,
			id = target.getAttribute(`data-toggle-${toggle}`);

			for (let i = 0; i <= this.displayedData.length; i++) {
				if (this.displayedData[i].id == id) {
					return this.displayedData[i];
				} else {
					console.log(`No items with id: ${id}`);
				}
			}
	}

	updateItem () {
		let	updatedItem = {
			url : document.querySelector('#newItemUrl').value,
			name : document.querySelector('#newItemName').value,
			id : document.querySelector('#newItemId').value,
			description : document.querySelector('#newItemDescription').value,
			date : moment(document.querySelector('#newItemDate').value, 'YYYY/MM/DD hh:mm')
		};

		let options = {
			method: 'put',
			headers: {"Content-type":"application/json;charset=UTF-8"}, 
			body: JSON.stringify(updatedItem)
		};
	 
		fetch(`http://localhost:3000/cars/${updatedItem.id}`, options).then(responce => responce.json())
            .then(data => {
				this.initComponent();
            }).catch((error) => console.log(error))
	}



	/*View */
	renderGallery () {
		this.sortingArray();
		this.renderResult(this.itemsOfGalleryContainer, this.fillingDataWithTemplateStrings());
		this.renderResult(this.quantity, this.displayedData.length);
	}

	fillFormWithUpdatingItem (item) {
		document.querySelector('#newItemUrl').value = item.url;
		document.querySelector('#newItemName').value = item.name;
		document.querySelector('#newItemId').value = item.id;
		document.querySelector('#newItemDescription').value = item.description;
		document.querySelector('#newItemDate').value = moment(item.date).format('YYYY/MM/DD hh:mm');
	}


									
	initComponent () {
		fetch("http://localhost:3000/cars").then(responce => responce.json())
		.then(data => {

			/*Model */
			this.displayedData = data;

			/*View */
			this.renderGallery();

			let editBtns = document.querySelectorAll('button[data-toggle-editItem]'),
			viewBtns = document.querySelectorAll('button[data-toggle-viewItem]');


			/*Controller */
			let	sortingOptionFunc = () => {
				this.saveSortingOptionToLS();
				this.renderGallery();
			};

			let	showItemFormBtnFunc = () => {
				serviceFunctions.show(this.itemForm, this.addItemBtn, this.hideItemFormBtn);
				serviceFunctions.hide(this.showItemFormBtn);
				this.setNewItemId();
			};

			let	addItemBtnFunc = () => {
				serviceFunctions.show(this.showItemFormBtn);
				serviceFunctions.hide(this.itemForm, this.addItemBtn, this.hideItemFormBtn);
				this.createItem();
			};

			let	updateItemBtnFunc = () => {
				serviceFunctions.show(this.showItemFormBtn);
				serviceFunctions.hide(this.itemForm, this.updateItemBtn, this.hideItemFormBtn);
				this.updateItem();
			};

			let	hideItemFormBtnFunc = () => {
				serviceFunctions.show(this.showItemFormBtn);
				serviceFunctions.hide(this.itemForm, this.updateItemBtn, this.addItemBtn, this.hideItemFormBtn);
				this.updateItem();
			};

			let	editItemBtnsFunc = (event) => {
				let item = this.returnUpdatingItem(event, "editItem");
				this.fillFormWithUpdatingItem(item);
				serviceFunctions.show(this.itemForm, this.updateItemBtn, this.hideItemFormBtn);
				serviceFunctions.hide(this.showItemFormBtn, this.addItemBtn);
			};

			let	viewItemBtnsFunc = (event) => {
				let item = this.returnUpdatingItem(event, "viewItem");
				this.fillFormWithUpdatingItem(item);
				serviceFunctions.show(this.itemForm, this.hideItemFormBtn);
				serviceFunctions.hide(this.showItemFormBtn);
			};

			this.sortingOption.addEventListener("change", sortingOptionFunc);
			this.showItemFormBtn.addEventListener("click", showItemFormBtnFunc);
			this.addItemBtn.addEventListener("click", addItemBtnFunc);
			this.updateItemBtn.addEventListener("click", updateItemBtnFunc);
			this.hideItemFormBtn.addEventListener("click", hideItemFormBtnFunc);

			for (let btn of editBtns) {
				btn.addEventListener("click", (event) => editItemBtnsFunc(event));
			}

			for (let btn of viewBtns) {
				btn.addEventListener("click", (event) => viewItemBtnsFunc(event));
			}
		})
	}
}

class ExtendedGallery extends BaseGallery {
	constructor () {
		super();
	}
	
	initComponent () {
		super.initComponent();

		/*Model */
		let deletingItems = (event) => {
			let target = event.target,
				id = target.getAttribute("data-toggle-id");
			if (!id) return;
			fetch('http://localhost:3000/cars/'+id, {method : 'delete'}).then(() => this.initComponent());
		};
		
		/*Controller */
		this.itemsOfGalleryContainer.addEventListener("click", (event) => deletingItems(event));
	}
}