'use strict'

class Validator {
    constructor (user) {
        /*Model */
        this.login = user.login;
        this.password = user.password;
        this.isEntered = JSON.parse(localStorage.getItem("isEntered"));
        this.emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        this.passwordRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

        /*View */
        this.alertBox = document.querySelector("#alert");
        this.inputedLogin = document.querySelector("#inputEmail");
        this.inputedPassword = document.querySelector("#inputPassword");
    }

    /*Model */
    validateInput (regex, element) {
        return regex.test(element.value);
    }

    showMsgInAlertBox (message) {
        this.alertBox.innerHTML = message;
    }

    compareLoginAndPassword () {
		if (this.inputedLogin.value === this.login 
				&& this.inputedPassword.value === this.password) {
			return true;
		} else {
			return false;
		}
    }

    check () {

        /*View */
        if (!this.isEntered) {
            if (!this.inputedLogin.value) {
                this.showMsgInAlertBox("Please enter the login");
                serviceFunctions.show(this.alertBox);
                return false;
            } else if (!this.inputedPassword.value) {
                this.showMsgInAlertBox("Please enter the password");
                serviceFunctions.show(this.alertBox);
                return false;
            } else if (!this.validateInput(this.emailRegEx, this.inputedLogin)) {
                this.showMsgInAlertBox("Login should be your email");
                serviceFunctions.show(this.alertBox);
                return false;
            } else if (!this.validateInput(this.passwordRegEx, this.inputedPassword)) {
                this.showMsgInAlertBox("Password should be at least 8 characters and contain one digit, one lower case and one upper case");
                serviceFunctions.show(this.alertBox);
                return false;
            } else if (!this.compareLoginAndPassword) {
                this.showMsgInAlertBox("Please register before sign in!");
                serviceFunctions.show(this.alertBox);
                return false;
            } else {
                serviceFunctions.hide(this.alertBox);
                localStorage.setItem("isEntered", "true");
                return true;
            }
        } else {
            serviceFunctions.hide(this.alertBox);
            return true;
        }
	}
}