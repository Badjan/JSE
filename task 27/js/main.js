'use strict'
let user = {
    login: "bk@g.com",
    password: "123456Asd"
},
    btn = document.getElementById("sign"),
    enterBtn = document.querySelector("#sign");

function enterProfile() {
    let validatorModule = new Validator();
    let galleryModule = new ExtendedGallery();
    let loginForm = new LoginForm(validatorModule, galleryModule);
    loginForm.initComponent();
}

function openEnteredProfile() {
    if (JSON.parse(localStorage.getItem("isEntered"))) {
        enterProfile();
    }
}

function setSortingOptionFromLS() {
    let sortingOption = document.querySelector('#sel1');
    sortingOption.value = localStorage.sortingOption;
}

localStorage.setItem("User", JSON.stringify(user));

enterBtn.addEventListener("click", enterProfile);
document.addEventListener("DOMContentLoaded", () => {
    openEnteredProfile();
    setSortingOptionFromLS();
});