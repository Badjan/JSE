let serviceFunctions = (function() {
    let show = (...elements) => {
        elements.forEach((element) => element.classList.remove("hide"));
    };

    let hide = (...elements) => {
        elements.forEach((element) => element.classList.add("hide"));
    };

    let higlight = (...elements) => {
        elements.forEach((element) => element.classList.add("higlite"));
    };

    let unhiglight = (...elements) => {
        elements.forEach((element) => element.classList.remove("higlite"));
    };

    function inheritance(parent, child) {
        let tempChild = child.prototype;
        child.prototype = Object.create(parent.prototype);
        child.prototype.constructor = child;
    
        for (let key in tempChild) {
            if (tempChild.hasOwnProperty(key)) {
                child.prototype[key] = tempChild[key];
            }
        }
    }

    return {
        show : show,
        hide : hide,
        inheritance : inheritance,
        higlight : higlight,
        unhiglight : unhiglight
    }
}());