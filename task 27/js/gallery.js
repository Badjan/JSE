'use strict'

let BaseGallery = function () {	
	this.addItemBtn = document.querySelector('#addItem');
	this.itemsOfGalleryContainer = document.querySelector('#itemsOfGalleryContainer');
	this.sortingOption = document.querySelector('#sel1');
	this.gallery = document.querySelector("#gallery");
	this.quantity = document.querySelector('#quantity'),
    this.displayedData = [];
    this.hiddenData = data.slice();
}

BaseGallery.prototype = {

	compareAlphabetical : function(a, b) {
		if (a.name.toLowerCase() > b.name.toLowerCase()) {
			return 1;
		} else {
			return -1;
		}
	},

	compareAlphabeticalReverse : function(a, b) {
		if (a.name.toLowerCase() < b.name.toLowerCase()) {
			return 1;
		} else {
			return -1;
		}
	},

	compareDate : function (a, b) {
		if (a.date > b.date) {
			return 1;
		} else {
			return -1
		}
	},

	compareDateReverse : function (a, b) {
		if (a.date < b.date) {
			return 1;
		} else {
			return -1
		}
	},
				
	setDate : function(date) {
		return moment(date).format('YYYY/MM/DD HH:mm');
	},
				
	setURL : function(url) {
		if(url.indexOf('http://') < 0) {
			return `http://${url}`;
		} else {
			return url;
		}
	},
				
	setNameCase : function (item) {
		if (!item) return;
		return `${item.charAt(0).toUpperCase()}${item.slice(1).toLowerCase()}`;
	},
				
	setDescription : function(item) {
		if (item.length > 15) {
			return `${item.substr(0, 15)} ...`;
		} else {
			return item;
		}
	},

	setUserData : function () {
		let userData = JSON.parse(localStorage.getItem("userData"));
		if (userData) {
			this.displayedData = userData.displayedData;
			this.hiddenData = userData.hiddenData;
		}
	},
			
	renderResult : function(element, result) {
		element.innerHTML = result;
	},
				
	fillDisplayedData : function () {
		if (this.hiddenData.length !== 0) {
			this.displayedData.push(this.hiddenData.pop());
		}
	},
				
	toggleEnablityOFBtn : function () {
		if (this.displayedData.length === data.length) {
			this.addItemBtn.classList.add("disabled");
			return true;
		} else {
			this.addItemBtn.classList.remove("disabled");
			return false;
		}
	},
				
	sortingArray : function () {
		let option = this.sortingOption.value;
		if (option === "0") {
			this.displayedData.sort(this.compareAlphabetical);
		} else if (option == 1) {
			this.displayedData.sort(this.compareAlphabeticalReverse);
		} else if (option == 2) {
			this.displayedData.sort(this.compareDateReverse);
		} else {
			this.displayedData.sort(this.compareDate);
		}
	},
				
	saveSortingOptionToLS : function () {
		localStorage.setItem("sortingOption", this.sortingOption.value);
	},
								
	fillingDataWithTemplateStrings : function () {
		let result = "";
		for (let i = 0; i < this.displayedData.length; i++) {
			let template = `<div class="col-md-4" id="${i}">\
								<div class="card mb-4 box-shadow">\
									<img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="${this.setNameCase(this.displayedData[i].name)}" src="${this.setURL(this.displayedData[i].url)}" data-holder-rendered="true" style="height: 225px; width: 100%; display: block;">\
									<div class="card-body">\
										<p class="card-text">${this.setDescription(this.displayedData[i].description)}</p>\
										<div class="d-flex justify-content-between align-items-center">\
											<div class="btn-group">\
												<button type="button" class="btn btn-outline-secondary">View</button>\
												<button type="button" class="btn btn-outline-secondary">Edit</button>\
											</div>\
											<a class="btn btn-danger" data-toggle-id="${i}">Удалить</a>\
											<small class="text-muted">${this.setDate(this.displayedData[i].date)}</small>\
										</div>\
									</div>\
								</div>\
							</div>`;
				
			result += template;
		}
		return result;
	},

	saveUserDataToLS : function() {
		let userData = {
			"displayedData" : this.displayedData,
			"hiddenData" : this.hiddenData
		};
		localStorage.setItem("userData", JSON.stringify(userData));
	},

	renderGallery : function() {
		this.renderResult(this.itemsOfGalleryContainer, this.fillingDataWithTemplateStrings());
		this.renderResult(this.quantity, this.displayedData.length);
	},
									
	initComponent : function() {
		let addFunctionalityToAddBtn = () => {
				this.fillDisplayedData();
				this.renderGallery();
				this.saveUserDataToLS();
				if (this.toggleEnablityOFBtn()) {
					$('#myModal').modal('show'); 
				}
		},
			addFunctionalityToSortingOption = () => {
				this.saveSortingOptionToLS();
				this.sortingArray();
				this.renderGallery();
				this.saveUserDataToLS();
		};

		this.setUserData();
		this.renderGallery();
		this.saveUserDataToLS();
		this.addItemBtn.addEventListener("click", addFunctionalityToAddBtn);
		this.sortingOption.addEventListener("change", addFunctionalityToSortingOption);
	},
}

let ExtendedGallery = function() {
	BaseGallery.apply(this);
}

ExtendedGallery.prototype = {

	initComponent : function() {
		BaseGallery.prototype.initComponent.apply(this);

		let deletingItems = (event) => {
			let target = event.target,
				id = target.getAttribute("data-toggle-id");
			if (!id) return;
			this.hiddenData.push(this.displayedData[id]);
			this.displayedData.splice(id, 1);
			this.renderGallery();
			this.saveUserDataToLS();
			this.toggleEnablityOFBtn();
		};
		this.itemsOfGalleryContainer.addEventListener("click", function() { deletingItems(event)}, false);
	},
}

serviceFunctions.inheritance(BaseGallery, ExtendedGallery);