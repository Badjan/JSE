'use strict'

let Validator = function () {
    this.login = JSON.parse(localStorage.getItem("User")).login;
    this.password = JSON.parse(localStorage.getItem("User")).password;
    this.isEntered = JSON.parse(localStorage.getItem("isEntered"));
    this.emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.passwordRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    this.alertBox = document.querySelector("#alert");
    this.inputedLogin = document.querySelector("#inputEmail");
    this.inputedEmail = document.querySelector("#inputPassword");
}

Validator.prototype = {
    
    validateInput : function (regex, element) {
        return regex.test(element.value);
    },

    showMsgInAlertBox : function (message) {
        this.alertBox.innerHTML = message;
    },

    compareLoginAndPassword : function() {
		if (this.inputedLogin.value === this.login 
				&& this.inputedEmail.value === this.password) {
			return true;
		} else {
			return false;
		}
    },

    check : function (){
        if (!this.isEntered) {
            if (!this.inputedLogin.value) {
                this.showMsgInAlertBox("Please enter the login");
                serviceFunctions.show(this.alertBox);
                return false;
            } else if (!this.inputedEmail.value) {
                this.showMsgInAlertBox("Please enter the password");
                serviceFunctions.show(this.alertBox);
                return false;
            } else if (!this.validateInput(this.emailRegEx, this.inputedLogin)) {
                this.showMsgInAlertBox("Login should be your email");
                serviceFunctions.show(this.alertBox);
                return false;
            } else if (!this.validateInput(this.passwordRegEx, this.inputedEmail)) {
                this.showMsgInAlertBox("Password should be at least 8 characters and contain one digit, one lower case and one upper case");
                serviceFunctions.show(this.alertBox);
                return false;
            } else if (!this.compareLoginAndPassword) {
                this.showMsgInAlertBox("Please register before sign in!");
                serviceFunctions.show(this.alertBox);
                return false;
            } else {
                serviceFunctions.hide(this.alertBox);
                localStorage.setItem("isEntered", "true");
                return true;
            }
        } else {
            serviceFunctions.hide(this.alertBox);
            return true;
        }
	}, 
}