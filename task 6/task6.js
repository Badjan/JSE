// part 2
  var firstVariable, secondVariable, thirdVariable;
  firstVariable = secondVariable = thirdVariable = "Hello";

  firstVariable = "Hi ";
  secondVariable = "to ";
  thirdVariable = "JSE!";

  console.log(firstVariable, secondVariable, thirdVariable);


//part 3
  var testingTypes;
  console.log(testingTypes);

  testingTypes = false;
  console.log(testingTypes);

  testingTypes = Number(testingTypes);
  console.log(testingTypes);

  testingTypes = String(testingTypes);
  console.log(testingTypes);

  testingTypes = null;
  console.log(testingTypes);

  console.log(typeof testingTypes);


//part 4
  let nameOfCourse = "JavaScript";
  var courses = "courses";
  let result = nameOfCourse + " " + courses;

  console.log(result);


//part 5
  let numberToIncrement = 1;

  numberToIncrement++;
  numberToIncrement++;
  numberToIncrement++;

  var z, x;

  z = x = numberToIncrement;


//part 6
  let someNumber = 0;
  someNumber = String(someNumber);
  someNumber = Boolean(someNumber);
  someNumber = Number(someNumber);
  
  console.log(someNumber);