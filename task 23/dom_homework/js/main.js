(function(){
'use strict'

let btn = document.getElementById("play"),
    gallery = document.querySelector('#gallery'),
    quantity = document.querySelector('#quantity'),
    sortingOption = document.querySelector('#sel1'),
    displayedData = [],
    hiddenData = data.slice();

function compareAlphabetical(a, b) {
    if (a.name.toLowerCase() > b.name.toLowerCase()) {
        return 1;
    } else {
        return -1;
    }
}

function compareAlphabeticalReverse(a, b) {
    if (a.name.toLowerCase() < b.name.toLowerCase()) {
        return 1;
    } else {
        return -1;
    }
}

function compareDate(a, b) {
    if (a.date > b.date) {
        return 1;
    } else {
        return -1
    }
}

function compareDateReverse(a, b) {
    if (a.date < b.date) {
        return 1;
    } else {
        return -1
    }
}

function setDate(date) {
    return moment(date).format('YYYY/MM/DD HH:mm');
}

function setURL(url) {
    if(url.indexOf('http://') < 0) {
        return `http://${url}`;
    } else {
        return url;
    }
}

function setNameCase(item) {
    if (!item) return;
    return `${item.charAt(0).toUpperCase()}${item.slice(1).toLowerCase()}`;
}

function setDescription(item) {
    if (item.length > 15) {
        return `${item.substr(0, 15)} ...`;
    } else {
        return item;
    }
}

function renderResult(element, result) {
    element.innerHTML = result;
}

function fillDisplayedData() {
    if (hiddenData.length !== 0) {
        displayedData.push(hiddenData.pop());
    }
}

function disableBtn() {
    btn.classList.add("disabled");
}

function checkIfBtnNeedToBeDisabled() {
    if (displayedData.length === data.length) {
        disableBtn();
        return true;
    }
}

function sortingArray() {
    let option = sortingOption.value;
    if (option === "0") {
        displayedData.sort(compareAlphabetical);
    } else if (option == 1) {
        displayedData.sort(compareAlphabeticalReverse);
    } else if (option == 2) {
        displayedData.sort(compareDateReverse);
    } else {
        displayedData.sort(compareDate);
    };
}

function saveSortingOptionToLocalStorage() {
    localStorage.setItem("sortingOption", sortingOption.value);
}

function setSortingOptionFromLocalStorage() {
    sortingOption.value = localStorage.sortingOption;
}

function fillingDataWithTemplateStrings() {
    let result = "";
    for (let i = 0; i < displayedData.length; i++) {
        let template = `<div class="col-sm-3 col-xs-6" id="${i}">\
                <img src="${setURL(displayedData[i].url)}" alt="${setNameCase(displayedData[i].name)}" class="img-thumbnail">\
                <div class="info-wrapper text-center">\
                    <div class="text-muted">${setNameCase(displayedData[i].name)}</div>\
                    <div class="text-muted top-padding">${setDescription(displayedData[i].description)}</div>\
                    <div class="text-muted">${setDate(displayedData[i].date)}</div>\
                    <div class="btn btn-danger delete" data-toggle-id="${i}">Удалить</div>
                </div>\
            </div>`;

        result += template;
    }
    return result;
}

function deletingItems(event) {
    let target = event.target,
        id = target.getAttribute("data-toggle-id"),
        elem;
    if (!id) return;
    hiddenData.push(displayedData[id]);
    displayedData.splice(id, 1);
}

function buildGallery() {
    sortingArray();
    renderResult(gallery, fillingDataWithTemplateStrings());
    renderResult(quantity, displayedData.length);
    if (checkIfBtnNeedToBeDisabled()) {
        $('#myModal').modal('show'); 
    }
}

function addFunctionalityToGallery(event) {
    deletingItems(event);
    sortingArray();
    renderResult(gallery, fillingDataWithTemplateStrings());
    renderResult(quantity, displayedData.length);
}

function addFunctionalityToBtn() {
    fillDisplayedData();
    buildGallery();
}

function addFunctionalityToSortingOption() {
    saveSortingOptionToLocalStorage();
    sortingArray();
    renderResult(gallery, fillingDataWithTemplateStrings());
    renderResult(quantity, displayedData.length);
}

btn.addEventListener("click", addFunctionalityToBtn);
gallery.addEventListener("click", addFunctionalityToGallery);
sortingOption.addEventListener("change", addFunctionalityToSortingOption);
document.addEventListener("DOMContentLoaded", setSortingOptionFromLocalStorage);

})()