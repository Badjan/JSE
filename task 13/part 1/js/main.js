'use strict'
var first,
    second,
    total = 0,
    result = '',
    element = document.getElementById("result");

function getRndNumber() {
  return Math.floor((Math.random() * 6) + 1);
}

function setIterationString(first, second) {
  let iteration = "Первая кость: " + first + " Вторая кость: " + second + "<br>";
  setResult(iteration);
}

function setResult(resultComponent) {
  result += resultComponent;
}

function isNumbersEqual(first, second) {
  let equalString = '';
  if (first === second) {
    equalString = "Выпал дубль. Число " + first + "<br>";
    setResult(equalString);
  }
}

function isBigDifference(first, second) {
  let differenceString = '';
  if ((first < 3 && second > 4) || (second < 3 && first > 4)) {
    differenceString = "Большой разброс между костями. Разница составляет " + Math.abs(first - second) + "<br>";
    setResult(differenceString);
  }
}

function calcTotal(first, second) {
  total += first + second;
}

function setTotalString() {
  let totalString = (total < 100 ? "Вы проиграли, у Вас " + total + " очков." : "Победа, вы набрали " + total + " очков!");
  setResult(totalString);
}

function printResult (path, stringToPrint) {
  path.innerHTML += stringToPrint;
}

(function run() {
  for (let i = 1; i <= 15; i++) {
    if (i === 8 || i === 13) {
      continue;
    }

    first = getRndNumber();
    second = getRndNumber();
    
    setIterationString(first, second);
    isNumbersEqual (first, second);
    isBigDifference (first, second);
    calcTotal (first, second);
  }
  setTotalString();
  printResult(element, result);
}());
