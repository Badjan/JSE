'use strict';
// task 1
var ravens = prompt("How much ravens?", 0);

(function setPropperEnding(ravens) {
    let basis = ravens + ' ворон',
        endingId = ravens.slice(-1);

    switch (endingId) {
        case '1': basis += 'a'; break;
        case '2':
        case '3':
        case '4':
                basis += 'ы'; break;
        default: break;
    }
    document.getElementById("result").innerHTML += basis + '<br>';
})(ravens);

// task 2
(function cutTheString() {
    let result =  prompt("Enter your string", "String should be more or less than 15 symbols");

    if (result.length > 15) {result = result.substring(0, 15) + '...'};

    document.getElementById("result").innerHTML += result + '<br>';
})();

// task 3
function isLeapYear (year) {
  if(!year) return 'Error - year is not found';
  let result = ((year % 400 === 0) || ((year % 4 === 0) && !(year % 100 === 0))) ? true : false;
  return result;
}

document.getElementById("result").innerHTML += isLeapYear(1600) + '<br>';
document.getElementById("result").innerHTML += isLeapYear(2100) + '<br>';
document.getElementById("result").innerHTML += isLeapYear(2012) + '<br>';
document.getElementById("result").innerHTML += isLeapYear() + '<br>';