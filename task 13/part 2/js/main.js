'use strict';
var btn = document.getElementById("play"),
    player1 = document.getElementById("player1"),
    player2 = document.getElementById("player2"),
    divWithResult = document.getElementById("result");

function getPlayerResult() {
    return Math.floor((Math.random() * 3) + 1);
}

function getNameById(id){
    switch(id) {
        case 1: return "Камень"; break;
        case 2: return "Ножницы"; break;
        case 3: return "Бумага"; break;
        default: return "Ошибка"; break;
    }
}

function determineWinner(first, second){
    if (first === second) return "Ничья";

    if (first === 1 && second === 2) return "Выиграл первый игрок";
    if (first === 2 && second === 3) return "Выиграл первый игрок";
    if (first === 3 && second === 1) return "Выиграл первый игрок";

    return "Выиграл второй игрок";
}

function runGame() {
    let player1Result = getPlayerResult(),
        player2Result = getPlayerResult();
    player1.innerHTML = getNameById(player1Result);
    player2.innerHTML = getNameById(player2Result);
    
    divWithResult.innerHTML = determineWinner(player1Result, player2Result);
}



btn.addEventListener("click", runGame);