"use strict";

let user = {
        login: "bk@g.com",
        password: "123456Asd"
    },
    btn = document.getElementById("sign");

localStorage.setItem("User", JSON.stringify(user));


function Register() {
    const form = document.querySelector("#form"),
        loginField = document.querySelector("#inputEmail"),
        passwordField = document.querySelector("#inputPassword"),
        alertField = document.querySelector(".alert"),
        page = document.querySelector("#page"),
        loginOnPageField = document.querySelector("#loginOnPage"),
        passwordOnPageField = document.querySelector("#passwordOnPage"),
        togglePasswordBtn = document.querySelector("#togglePasswordBtn"),
        backBtn = document.querySelector("#back"),
        emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        passwordRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

/* password:
  (?=.*\d)          // should contain at least one digit
  (?=.*[a-z])       // should contain at least one lower case
  (?=.*[A-Z])       // should contain at least one upper case
  [a-zA-Z0-9]{8,}   // should contain at least 8 from the mentioned characters
*/

    let show = (element) => element.classList.remove("hide");

    let hide = (element) => element.classList.add("hide");

    let hidePage = () => {hide(page); show(form);}

    let checkField = (field) => {
        if (field.value) {
            return true;
        } else {
            return false;
        }
    };

    let validateInput = function(regex, element) {
        return regex.test(element.value);
    };

    let compareLoginAndPassword = function() {
        let loginFromStorage = JSON.parse(localStorage.getItem("User")).login,
            passwordFromStorage = JSON.parse(localStorage.getItem("User")).password;
        if (loginField.value == loginFromStorage 
                && passwordField.value == passwordFromStorage) {
            return true;
        } else {
            return false;
        }
    };

    let fillInLoginAndPasswordInPage = function() {
        loginOnPageField.value = loginField.value;
        passwordOnPageField.value = passwordField.value;
    };

    let changeVisibilityOfPassword = function() {
        if (passwordOnPageField.getAttribute("type") === "password") {
            passwordOnPageField.setAttribute("type", "text");
        } else {
            passwordOnPageField.setAttribute("type", "password");
        }
    };

    let changeTextIntogglePasswordBtn = function() {
        if (passwordOnPageField.getAttribute("type") === "password") {
            togglePasswordBtn.innerHTML = "Show password";
        } else {
            togglePasswordBtn.innerHTML = "Hide password";
        }
    };

    let togglePasswordBtnEvents = function() {
        changeVisibilityOfPassword();
        changeTextIntogglePasswordBtn();
    };

    let initListeners = function () {
        togglePasswordBtn.addEventListener("click", togglePasswordBtnEvents);
        back.addEventListener("click", hidePage);
    };
   
    this.setLogAndPass = function(user) {
        localStorage.setItem("User", JSON.stringify(user));
    }

    this.initComponent = function() {
        let checkFields = checkField(loginField) 
                            && checkField(passwordField) 
                            && validateInput(emailRegEx, loginField) 
                            && validateInput(passwordRegEx, passwordField)
                            && compareLoginAndPassword();
        
        initListeners();
        
        if (checkFields) {
            fillInLoginAndPasswordInPage();
            hide(alertField);
            hide(form);
            show(page);
        } else {
            show(alertField);
        }
    }
}

let reg = new Register();

function getUserData() {
    reg.setLogAndPass(user);
    reg.initComponent();
}

btn.addEventListener("click", getUserData);