(function(){
'use strict'

let btn = document.getElementById("play"),
    firstBlock = document.querySelector('#first-line'),
    secondBlock = document.querySelector('#second-line'),
    thirdBlock = document.querySelector('#third-line'),
    typeSelector = document.getElementById("type-selector"),
    lineSelector = document.getElementById("line-selector");

function setDate(date) {
    return moment(date).format('YYYY/MM/DD HH:mm');
}

function setURL(url) {
    if(url.indexOf('http://') < 0) {
        return `http://${url}`;
    } else {
        return url;
    }
}

function setNameCase(item) {
    if (!item) return;
    return `${item.charAt(0).toUpperCase()}${item.slice(1).toLowerCase()}`;
}

function setDescription(item) {
    if (item.length > 15) {
        return `${item.substr(0, 15)} ...`;
    } else {
        return item;
    }
}

function clearContentInBlocks() {
    for(let i = 0; i < arguments.length; i++) {
        arguments[i].innerHTML = "";
    }
}

function hideAllGroups() {
    for(let i = 0; i < arguments.length; i++) {
        arguments[i].classList.add("hide");
    }
}

function renderResult(element, result) {
    element.innerHTML = result;
}

function showExactGroup(groupNumber) {
    let firstGroup = document.querySelector('.first-group'),
        secondGroup = document.querySelector('.second-group'),
        thirdGroup = document.querySelector('.third-group');

    hideAllGroups(firstGroup, secondGroup, thirdGroup);

    if (groupNumber === '2') return secondGroup.classList.toggle("hide");
    if (groupNumber === '3') return thirdGroup.classList.toggle("hide");
    firstGroup.classList.toggle("hide");
}

function fillingDataWithReplace(times) {
    let result = "", 
        replaceItemTemplate = '<div class="col-sm-3 col-xs-6">\
            <img src="$url" alt="$name" class="img-thumbnail">\
            <div class="info-wrapper">\
                <div class="text-muted">$name</div>\
                <div class="text-muted top-padding">$description</div>\
                <div class="text-muted">$date</div>\
            </div>\
        </div>';

    for (let i = 0; i < times; i++) {
        let resultHTML = replaceItemTemplate
            .replace(/\$name/gi, setNameCase(data[i].name))
            .replace("$url", setURL(data[i].url))
            .replace("$description", setDescription(data[i].description))
            .replace("$date", setDate(data[i].date));
    
        result += resultHTML;
    }
    return result;
}

function fillingDataWithTemplateStrings(times) {
    let result = "";
    for (let i = 0; i < times; i++) {
        let secondItemTemplate = `<div class="col-sm-3 col-xs-6">\
                <img src="${setURL(data[i].url)}" alt="${setNameCase(data[i].name)}" class="img-thumbnail">\
                <div class="info-wrapper">\
                    <div class="text-muted">${setNameCase(data[i].name)}</div>\
                    <div class="text-muted top-padding">${setDescription(data[i].description)}</div>\
                    <div class="text-muted">${setDate(data[i].date)}</div>\
                </div>\
            </div>`;

        result += secondItemTemplate;
    }
    return result;
}

function fillingDataWithCreateElement(times){
    let result = "";
    for (let i = 0; i < times; i++) {
        let column = document.createElement('div'),
            img = document.createElement('img'),
            wrapper = document.createElement('div'),
            divWithName = document.createElement('div'),
            divWithDescription = document.createElement('div'),
            divWithDate = document.createElement('div'),
            name = document.createTextNode(setNameCase(data[i].name)),
            description = document.createTextNode(setDescription(data[i].description)),
            date = document.createTextNode(setDate(data[i].date));

        column.classList.add('col-sm-3', 'col-xs-6');
        img.classList.add('img-thumbnail');
        img.setAttribute('src', setURL(data[i].url));
        img.setAttribute('alt', setNameCase(data[i].name));
        wrapper.classList.add('info-wrapper');
        divWithName.classList.add('text-muted');
        divWithDescription.setAttribute('class', 'text-muted top-padding');
        divWithDate.classList.add('text-muted');

        divWithName.appendChild(name);
        divWithDescription.appendChild(description);
        divWithDate.appendChild(date);

        wrapper.appendChild(divWithName);
        wrapper.appendChild(divWithDescription)
        wrapper.appendChild(divWithDate);
        column.appendChild(img);
        column.appendChild(wrapper);
        result += column;
    }
}

function createGalleryWithReplace(times, group) {
    clearContentInBlocks(firstBlock, secondBlock, thirdBlock);
    showExactGroup(group);
    renderResult(firstBlock, fillingDataWithReplace(times));
}

function createGalleryWithTemplateStrings(times, group) {
    clearContentInBlocks(firstBlock, secondBlock, thirdBlock);
    showExactGroup(group);
    renderResult(secondBlock, fillingDataWithTemplateStrings(times));
}

function createGalleryWithCreateElement(times, group) {
    clearContentInBlocks(firstBlock, secondBlock, thirdBlock);
    showExactGroup(group);
    renderResult(thirdBlock, fillingDataWithTemplateStrings(times));
}

function buildGallery(type, quantity) {
    let lineSelector;

    if(quantity === '0'){
        lineSelector = data.length;
    } else if(quantity === '1') {
        lineSelector = 3;
    } else {
        lineSelector = 6;
    }

    if(type === '2') {
        createGalleryWithTemplateStrings(lineSelector, type);
    } else if(type === '3') {
        createGalleryWithCreateElement(lineSelector, type);
    } else {
        createGalleryWithReplace(lineSelector, type);
    }
}

function init() {
    let type = typeSelector.value,
        line = lineSelector.value;
    buildGallery(type, line);
}

btn.addEventListener("click", init);

})()