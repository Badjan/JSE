'use strict';

let serviceFunctions = (function() {
    let show = (...elements) => {
        elements.forEach((element) => element.classList.remove("hide"));
    };

    let hide = (...elements) => {
        elements.forEach((element) => element.classList.add("hide"));
    };

    let higlight = (...elements) => {
        elements.forEach((element) => element.classList.add("higlite"));
    };

    let unhiglight = (...elements) => {
        elements.forEach((element) => element.classList.remove("higlite"));
    };

    return {
        show : show,
        hide : hide,
        higlight : higlight,
        unhiglight : unhiglight
    }
}());