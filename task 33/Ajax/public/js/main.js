'use strict'

let enterBtn = document.querySelector("#sign");

let enterProfile = function () {
    let enteredLogin = document.querySelector("#inputEmail").value,
        enteredPassword = document.querySelector("#inputPassword").value,
        alertBox = document.querySelector("#alert"),
        userCredentials = {
            login: enteredLogin,
            password: enteredPassword
        },
        options = {
            method: 'post',
			headers: {"Content-type":"application/json;charset=UTF-8"}, 
			body: JSON.stringify(userCredentials)
        };

    fetch(`http://localhost:3000/login`, options).then(response => response.json())
            .then(data => {
                if (data.status) {
                    serviceFunctions.hide(alertBox);
                    let validatorModule = new Validator(userCredentials);
                    let galleryModule = new ExtendedGallery();
                    let loginForm = new LoginForm(validatorModule, galleryModule);
                    loginForm.initComponent();
                }
            }).catch(
                serviceFunctions.show(alertBox)
            )
}

function openEnteredProfile() {
    if (JSON.parse(localStorage.getItem("isEntered"))) {
        enterProfile();
    }
}

function setSortingOptionFromLS() {
    let sortingOption = document.querySelector('#sel1');
    sortingOption.value = localStorage.sortingOption;
}

enterBtn.addEventListener("click", enterProfile);
document.addEventListener("DOMContentLoaded", () => {
    openEnteredProfile();
    setSortingOptionFromLS();
});