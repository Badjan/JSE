'use strict';

let LoginForm = function (validatorModule, galleryModule) {
	this.validatorModule = validatorModule;
	this.galleryModule = galleryModule;
	this.form = document.querySelector("#form");
	this.page = document.querySelector("#page");
    this.loginOnPageField = document.querySelector("#loginOnPage");
    this.passwordOnPageField = document.querySelector("#passwordOnPage");
	this.togglePasswordBtn = document.querySelector("#togglePasswordBtn");
	this.menuButtons = document.querySelector("#menuButtons");
	this.galleryBtn = document.querySelector("#galleryBtn");
	this.userBtn = document.querySelector("#userBtn");
	this.exit = document.querySelector("#exit");
	this.gallery = this.galleryModule.gallery;
}

LoginForm.prototype = {

	fillLoginAndPasswordInPage : function() {
		this.loginOnPageField.value = this.validatorModule.login;
		this.passwordOnPageField.value = this.validatorModule.password;
	},

	changeVisibilityOfPassword : function() {
		if (this.passwordOnPageField.getAttribute("type") === "password") {
			this.passwordOnPageField.setAttribute("type", "text");
			this.togglePasswordBtn.innerHTML = "Hide password";
		} else {
			this.passwordOnPageField.setAttribute("type", "password");
			this.togglePasswordBtn.innerHTML = "Show password";
		}
	},

	initComponent : function () {
		let togglePasswordFunctionality = () => {
			this.changeVisibilityOfPassword();
		};

		let exitBtnFunctionality = () => {
			serviceFunctions.hide(this.page, this.gallery, this.menuButtons);
			serviceFunctions.show(this.form);
			serviceFunctions.unhiglight(this.galleryBtn, this.userBtn);
			this.userBtn.removeEventListener("click", profileBtnFunctionality);
			this.galleryBtn.removeEventListener("click", galleryBtnFunctionality);
			this.togglePasswordBtn.removeEventListener("click", togglePasswordFunctionality);
			localStorage.removeItem("isEntered");
		};

		let profileBtnFunctionality = () => {
			serviceFunctions.hide(this.form, this.gallery);
			serviceFunctions.show(this.page);
			serviceFunctions.unhiglight(this.galleryBtn);
			serviceFunctions.higlight(this.userBtn);
		};

		let galleryBtnFunctionality = () => {
			serviceFunctions.hide(this.form, this.page);
			serviceFunctions.show(this.gallery);
			serviceFunctions.unhiglight(this.userBtn);
			serviceFunctions.higlight(this.galleryBtn);
		};

		if (this.validatorModule.check()) {
			serviceFunctions.hide(this.form);
			serviceFunctions.show(this.gallery, this.menuButtons);
			serviceFunctions.higlight(this.galleryBtn);
			this.fillLoginAndPasswordInPage();
			this.exit.addEventListener("click", exitBtnFunctionality);
			this.userBtn.addEventListener("click", profileBtnFunctionality);
			this.galleryBtn.addEventListener("click", galleryBtnFunctionality);
			this.togglePasswordBtn.addEventListener("click", togglePasswordFunctionality);
			this.galleryModule.initComponent();
		};
	},
}