'use strict'
var total = 0,
    first,
    second,
    result = '',
    element = document.getElementById("result");

for (let i = 1; i <= 15; i++) {
    if (i === 8 || i === 13) {
      continue;
    }

  first = Math.floor((Math.random() * 6) + 1);
  second = Math.floor((Math.random() * 6) + 1);

  console.log(i);

  result += "Первая кость: " + first + " Вторая кость: " + second + "<br>";

  if (first === second) {
    result += "Выпал дубль. Число " + first + "<br>";
  }

  if ((first < 3 && second > 4) || (second < 3 && first > 4)) {
    result += "Большой разброс между костями. Разница составляет " + Math.abs(first - second) + "<br>";
  }

  total += first + second;
}

result += (total < 100 ? "Вы проиграли, у Вас " + total + " очков." : "Победа, вы набрали " + total + " очков!");

element.innerHTML = result;