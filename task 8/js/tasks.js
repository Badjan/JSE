// task 1
  for (let i = 1; i <= 7; i++) {
     console.log("Квадрат " + i + " = " + i**2);
   };


// task 2
  var startingString = "Число ",
      odd = ". Нечетное",
      even = ". Четное",
      i = 1,
      result = '',
      secondResult = document.getElementById("secondResult");

  for (let i = 1 ; i <= 15; i++) {
    if ((i % 2) === 0) {
      console.log(startingString + i + even);
    } else {
      console.log(startingString + i + odd);
    }
  }

  while (i <= 15) {
    if ((i % 2) === 0) {
      console.log(startingString + i + even);
    } else {
      console.log(startingString + i + odd);
    }
    i++;
  }

  i = 1;
  do {
    if ((i % 2) === 0) {
      console.log(startingString + i + even);
    } else {
      console.log(startingString + i + odd);
    }
    i++;
  } while (i <= 15)


// task 3
  for (let i = 100; i >= 0; i -= 10) {
    if (i===90 || i===70 || i===30) {
      continue;
    }
    result += "Число : <b>" + i + "</b><br>";
  }

  secondResult.innerHTML = result;